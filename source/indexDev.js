'use strict'

// Overweride glimpse
require('./glimpse')
// Load glimpse
require('@glimpse/glimpse').init()
// Require main app
require('./').app
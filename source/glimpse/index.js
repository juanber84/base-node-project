"use strict"

Object.defineProperty(exports, "__esModule", { value: true })
var glimpse_common_1 = require("@glimpse/glimpse-common")
var importResourceRuntime = require("@glimpse/glimpse-server/release/src/resources/ResourceRuntime")
const allowedHosts = ['localhost', '127.0.0.1', '::1']
var ResourceRuntime = (function () {
    function ResourceRuntime(resourceAuthorization, resourceManager, errorReportingService) {
        this.resourceAuthorization = resourceAuthorization
        this.resourceManager = resourceManager
        this.errorReportingService = errorReportingService
    }
    ResourceRuntime.prototype.processRequest = function (req, res, next) {
        let host = req.connection.remoteAddress || req.headers['x-forwarded-for']
        if (allowedHosts.indexOf(host) == -1) {
            res.sendStatus(401)
        }
        var _this = this
        var result = this.resourceManager.match(req)
        if (result) {
            this.resourceAuthorization.canExecute(req, result.resource.type, function (err, canExecute) {
                if (err) {
                    _this.errorReportingService.reportError(glimpse_common_1.createAuthorizationInvocationFailedError(err))
                    res.sendStatus(500)
                }
                else if (canExecute) {
                    result.resource.invoke(req, res, next, result.parameters)
                }
                else {
                    // TODO: Review, do we want a 401, 404 or continue users pipeline (see #81)
                    res.sendStatus(401)
                }
            })
        }
        else {
            res.sendStatus(404)
        }
    }
    return ResourceRuntime
}())

importResourceRuntime.ResourceRuntime = ResourceRuntime

'use strict'

const exceptions = require('../exceptionPool')

let services = []
let parameters = []

module.exports = {
    get(name){
        return services[name]
    },
    set(name, path){
        services[name] = require(path)
    },
    setParameters(params){
        parameters = params
    },
    getParameter(param) {
        return parameters[param]
    },
    getException(exception){
        return new exceptions[exception]()
    }
}
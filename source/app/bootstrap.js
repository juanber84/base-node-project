'use strict'

const morgan = require('morgan')
const compression = require('compression')
const cors = require('cors')
const bodyParser = require('body-parser')
const expressDeliver = require('express-deliver')
const helmet = require('helmet')
// const redisClient = require('./fn/redisClient')
const containerServices = require('./services/container')

module.exports = app => {

    app.set('x-powered-by', false)
    app.set('etag', false)

    app.use(helmet())

    expressDeliver(app, {
        printInternalErrorData: true,
        exceptionPool: require('./exceptionPool')
    })

    app.use(morgan('dev'))
    app.use(compression())
    app.use(cors())

    //Throw error if no redis connection
    app.use(function (req, res, next) {
        // if (!redisClient.connected)
        //     throw new res.exception.NoDatabase()
        next()
    })

    app.use(function(req, res, next){
        req.container = containerServices
        next()
    })

    //Parses http body
    app.use(bodyParser.urlencoded({ extended: true }))
    app.use(bodyParser.json())

    require('./routes')(app)

    expressDeliver.errorHandler(app)
}
const util = require('util')

/**
 * Use util.inspect to print a pretty object 
 * @param  {Mixed} obj 
 * @return {void}     
 */
function inspect(obj){
    return util.inspect(obj,{
        depth : 30,
        colors : true
    })
}

/* eslint-disable no-console */
console.inspect = function(){
    console.log.apply(console,Array.prototype.slice.call(arguments).map(inspect))
}
/* eslint-enable no-console */

/**
 * util.inspect improvements
 */
//Define colors
util.inspect.styles = {
    special: 'cyan',
    number: 'green',
    boolean: 'green',
    undefined: 'cyan',
    null: 'cyan',
    string: 'yellow',
    symbol: 'magenta',
    date: 'magenta',
    regexp: 'green',
    name: 'bold'
}
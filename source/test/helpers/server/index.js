// Include inspect
require('./inspect')
// Test overwrite
require('./mock')
// Require main app
const index = require('./../../../')
// Export app to test
exports.app = index.app
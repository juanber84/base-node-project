const debug = require('debug')('app:test:mock')
// const _ = require('lodash')
const mock = require('mock-require')

// Mock parameters
debug('parameters-change')
mock('./../../parameters.json', require('../parameters.json'))

//  Mock express
if (process.env.LISTEN != 1) {
    debug('express-server-invalidate-listen')
    let express = require('express')
    express.application.listen = function () {
        return
    }
    mock('express', express)
}

// Mock morgan
if (process.env.LOGS != 1) {
    debug('invalidate-morgan')
    mock('morgan', function () {
        return function (req, res, next) {
            next()
        }
    })
}
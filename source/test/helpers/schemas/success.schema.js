const successSchema = {
    type: "object",
    properties: {
        status: {
            type: "boolean",
            enum: [true]
        },
        data: {
            type: "object",
        }
    },
    required: ["status", "data"],
    additionalProperties: false
}

module.exports = successSchema
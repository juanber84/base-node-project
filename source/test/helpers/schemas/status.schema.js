import statusSchema from './success.schema'

// Modificate data
statusSchema.properties.data = {
    type: "string",
    enum: ['hi']
}

module.exports = statusSchema
let app = require('./server/').app
let request = require('supertest').agent(app)
const Validator = require('jsonschema').Validator

module.exports = {

    async requestMacro(t, obj, ...validations) {

        if (typeof obj !== 'object')
            throw new Error('obj must be a object')
        
        validations.forEach(f => {
            if (typeof f !== 'function')
                throw new Error('f must be a function')
        })

        // Generate request
        let req = request[obj.method](obj.url)
        // Set body
        if(obj.body)
            req.send(obj.body)
        // Set headers
        if(obj.headers) 
            req.set(obj.headers)
        // Send request
        let response = await req
            .expect('Content-type', /json/)
            .then(response => {
                return response
            })
            .catch(err => {
                throw err
            })

        // Validate json schema
        let v = new Validator()
        let validate = v.validate(response.body, obj.responseSchema)
        t.is(validate.valid, true, validate.errors)

        // Throw obj validation
        if (typeof obj.validations == 'function')
            await obj.validations(t, response.body)

        // Throw all extra validations
    }

}
import test from 'ava'
import macro from './../helpers/macro'

import statusSchema from './../helpers/schemas/status.schema'

test('/', macro.requestMacro, {
    name: '',
    description: '',
    url: '/',
    method: 'get',
    params: {},
    body: {},
    responseSchema: statusSchema,
    // statusCode: 200,
    async validations(t, data) {
        // t.is(data, 'bar')
    }
})

test('/status', macro.requestMacro, {
    name: '',
    description: '',
    url: '/status',
    method: 'get',
    params: {},
    body: {},
    responseSchema: statusSchema,
    // statusCode: 200,
    async validations(t, data) {
        // t.is(data, 'bar')
    }
})
'use strict'

const debug = require('debug')('app')
debug('init')

const parameters = require('./parameters')
const express = require('express')
const app = express()

const services = require('./app/services/includes.json')
const containerServices = require('./app/services/container')
containerServices.setParameters(parameters)
services.forEach(function (service) {
    containerServices.set(service.name, service.path)
})

require('./app/bootstrap')(app)

app.listen(parameters.listenPort, () => {
    debug('Listening', parameters.listenPort)
})

exports.app = app